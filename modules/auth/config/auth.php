<?php

return [

	'driver'  => 'File',
	'hash_method' => 'sha256',
	'hash_key' => 2,
	'lifetime' => 1209600,
	'session_type' => Session::$default,
	'session_key' => 'auth_user',
	// For Auth_Bcrypt
	'cost' => 10,
	// Username/password combinations for the Auth File driver
	'users' => [
		'admin' => '0b4bb8a3e720c286664843ba5c19e08e2b6eddede2e64a86222a53b7ddc80c14',
		'test' => '0b4bb8a3e720c286664843ba5c19e08e2b6eddede2e64a86222a53b7ddc80c14'
	],

];
