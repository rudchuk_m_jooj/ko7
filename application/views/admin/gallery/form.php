<form method="POST" action="" enctype="multipart/form-data">
  <div class="card">
    <div class="card-header text-right">
      <button type="submit" class="btn btn-sm btn-primary">Save</button>
      <a href="<?= URL::site('admin/gallery'); ?>" class="btn btn-sm btn-secondary">Close</a>
    </div>
    <div class="card-body">
      <div class="form-check">
        <label>Status: </label>
        <label class="form-check-label">
          <input class="form-check-input" type="radio" name="FORM[status]" id="status" value="0" <?= !isset($result) || $result['status'] === '0' ? 'checked' : ''; ?>>
          No
        </label>
        <label class="form-check-label">
          <input class="form-check-input" type="radio" name="FORM[status]" id="status" value="1" <?= $result['status'] === '1' ? 'checked' : ''; ?>>
          Yes
        </label>
      </div>
      <div class="form-group">
        <label for="f_name">Name</label>
        <input type="text" required class="form-control" id="f_name" name="FORM[name]" value="<?= $result['name']; ?>">
      </div>
      <div class="form-group">
        <label for="f_image">Image</label>
        <?php if (is_file(IMGPATH.'gallery'.DS.$result['image'])): ?>
          <div class="text-center">
            <img src="<?= URL::site('images/gallery/big/'.$result['image']); ?>" class="rounded" alt=""><br>
            <a href="<?= URL::site('admin/gallery/deleteImg/'.$result['id']); ?>" class="btn btn-danger" style="padding: 7px 100px;margin-top: 5px;">Delete</a>
          </div>
        <?php else: ?>
          <input type="file" name="file" class="form-control-file" id="f_image">
        <?php endif ?>
      </div>
    </div>
    <div class="card-footer text-right">
      <button type="submit" class="btn btn-sm btn-primary">Save</button>
      <a href="<?= URL::site('admin/gallery'); ?>" class="btn btn-sm btn-secondary">Close</a>
    </div>
  </div>
</form>