<!DOCTYPE html>
<html>
<head>
  <title>Auth</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
</head>
<body>
  <div class="container">
    <form method="POST" action="">
      <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" id="name" name="name">
      </div>
      <div class="form-group">
        <label for="password">Password</label>
        <input type="password" class="form-control" id="password" name="password">
      </div>
      <button type="submit" class="form-control btn btn-primary">Submit</button>
    </form>
    <?php if (isset($error)): ?>
      <div class="alert alert-danger" role="alert" style="margin-top: 20px;"><?= $error; ?></div>
    <?php endif ?>
  </div>
</body>
</html>