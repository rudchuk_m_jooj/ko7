<form method="POST" action="">
  <div class="card">
    <div class="card-header text-right">
      <button type="submit" class="btn btn-sm btn-primary">Save</button>
    </div>
    <div class="card-body">
      <div class="form-group">
        <label for="f_name">Name</label>
        <input type="text" required class="form-control" id="f_name" name="FORM[name]" value="<?= $result['name']; ?>">
      </div>
      <div class="form-group">
        <label for="f_text">Text</label>
        <textarea class="form-control tinymceEditor" id="f_text" rows="20" name="FORM[text]"><?= $result['text']; ?></textarea>
      </div>
    </div>
    <div class="card-footer text-right">
      <button type="submit" class="btn btn-sm btn-primary">Save</button>
    </div>
  </div>
</form>