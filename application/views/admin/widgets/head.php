<title><?= isset($title) ? $title : 'Default Title'; ?></title>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">

<link rel="stylesheet" href="<?= URL::site('css/font-awesome.min.css'); ?>">

<style>
  .card { margin-top: 20px; }
</style>

<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=ta8noesjlnv2oh5r2ksp2zs1ijorc7uf4zjlr8f7ygwdpd0w"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

<script>
window.onload = function() {
  if($('.tinymceEditor').length) {
    tinymce.init({
      selector: 'textarea.tinymceEditor',
      plugins: [
        'advlist autolink lists link image charmap print preview anchor',
        'searchreplace visualblocks code fullscreen',
        'insertdatetime media table contextmenu paste code'
      ],
      toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
      content_css: [
        '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
        '//www.tinymce.com/css/codepen.min.css']
    });
  }
  if($('#datepicker').length) {
    $('#datepicker').datepicker();
  }
}
</script>