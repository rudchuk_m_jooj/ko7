<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item"> <!-- active -->
        <a class="nav-link" href="<?= URL::site('admin'); ?>">Index</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= URL::site('admin/news'); ?>">News</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= URL::site('admin/gallery'); ?>">Gallery</a>
      </li>
    </ul>
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="<?= URL::site('auth/logout'); ?>">Exit</a>
      </li>
    </ul>
  </div>
</nav>