<div class="card">
  <div class="card-header text-right">
    <a href="<?= URL::site('admin/news/add'); ?>" class="btn btn-sm btn-success">Add</a>
  </div>
  <?php if (isset($result) && count($result)): ?>
  <table class="table table-striped table-hover">
	  <thead>
	    <tr>
	      <th>#</th>
	      <th>Name</th>
	      <th>Date</th>
	      <th width="4%"></th>
	      <th width="4%"></th>
	    </tr>
	  </thead>
	  <tbody>
      <?php foreach ($result as $val): ?>
	    <tr>
	      <th scope="row"><?= $val['id']; ?></th>
	      <td><?= $val['name']; ?></td>
	      <td><?= date('m/d/Y', $val['date']); ?></td>
	      <td><a href="<?= URL::site('admin/news/edit/'.$val['id']); ?>" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a></td>
	      <td><a href="<?= URL::site('admin/news/delete/'.$val['id']); ?>" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a></td>
	    </tr>
      <?php endforeach ?>
	  </tbody>
	</table>
  <?php else: ?>
    <div class="text-center">
  		<p>Пусто</p>
  	</div>
  <?php endif ?>
</div>