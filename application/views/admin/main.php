<!DOCTYPE html>
<html>
<head>
  <?= $head; ?>
</head>
<body>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <?= $header; ?>
      </div>
    </div>
    <?= $content; ?>
  </div>
</body>
</html>