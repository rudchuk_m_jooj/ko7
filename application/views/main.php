<!DOCTYPE html>
<html>
<head>
  <?= $head; ?>
</head>
<body>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <?= $header; ?>
      </div>
    </div>
    <?= $content; ?>
  </div>
  <script src="<?= URL::site('js/lightbox-plus-jquery.min.js'); ?>"></script>
</body>
</html>