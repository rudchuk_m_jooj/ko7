<?php if (isset($result) && count($result)): ?>
	<div class="card mycard">
		<div class="card-body">
			<div class="row">
				<?php foreach ($result as $val): ?>
					<div class="col-md-4">
					  <div class="card">
					  	<?php 
					  	if (is_file(IMGPATH.'news'.DS.$val['image'])) $i = URL::site('images/news/big/'.$val['image']);
					  	else $i = URL::site('pic/no-image.png');
					  	?>
						  <img class="card-img-top" src="<?= $i; ?>" alt="">
						  <div class="card-body">
						  	<a href="<?= URL::site('news/'.$val['id']); ?>">
						    	<p class="card-text"><?= $val['name']; ?></p>
						    </a>
						  </div>
						</div>
					</div>
				<?php endforeach ?>
			</div>
		</div>
		<div class="card-footer text-right">
		  <a href="<?= URL::site('news'); ?>" class="btn btn-sm btn-secondary">All News</a>
		</div>
	</div>
<?php endif ?>