<?php if (isset($result) && count($result)): ?>
	<div class="card mycard">
		<div class="card-body">
			<div class="row">
				<?php foreach ($result as $img): ?>
					<div class="col-md-6">
					  <div class="card">
					  	<?php 
					  	if (is_file(IMGPATH.'gallery'.DS.$img['image'])) $i = URL::site('images/gallery/big/'.$img['image']);
					  	else $i = URL::site('pic/no-image.png');
					  	?>
						  <img class="card-img-top" src="<?= $i; ?>" alt="">
						  <!-- <div class="card-body">
						    <p class="card-text"><?//= $img['name']; ?></p>
						  </div> -->
						</div>
					</div>
				<?php endforeach ?>
			</div>
		</div>
		<div class="card-footer text-right">
		  <a href="<?= URL::site('gallery'); ?>" class="btn btn-sm btn-secondary">All Image</a>
		</div>
	</div>
<?php endif ?>