<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item"> <!-- active -->
        <a class="nav-link" href="<?= URL::site(); ?>">Index</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= URL::site('news'); ?>">News</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= URL::site('gallery'); ?>">Gallery</a>
      </li>
    </ul>
  </div>
</nav>