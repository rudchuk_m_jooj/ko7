<div class="card mycard">
	<div class="card-body">
		<div class="row">
	    <?php if (isset($result) && count($result)): ?>
	      <?php foreach ($result as $val): ?>
	      	<div class="col-md-6">
					  <div class="card">
	        		<?php 
					  	if (is_file(IMGPATH.'gallery'.DS.$val['image'])) {
					  		$ib = URL::site('images/gallery/big/'.$val['image']);
					  		$io = URL::site('images/gallery/'.$val['image']);
					  	} else $ib = $io = URL::site('pic/no-image.png');
					  	?>
					  		<a href="<?= $io; ?>" data-lightbox="roadtrip">
						  		<img class="card-img-top" src="<?= $ib; ?>" alt="">
						  	</a>
						  <div class="card-body">
						    <p class="card-text"><?= $val['name']; ?></p>
						  </div>
						</div>
					</div>
	      <?php endforeach ?>
	    <?php else: ?>
	      <div class="text-center">
		  		<p>Пусто</p>
		  	</div>
	    <?php endif ?>
    </div>
  </div>
  <div class="card-footer">
	  <a href="<?= URL::site(); ?>" class="btn btn-sm btn-secondary">Back</a>
	</div>
</div>