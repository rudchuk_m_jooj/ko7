<div class="card mycard">
	<div class="card-body">
		<div class="row">
	    <?php if (isset($result) && count($result)): ?>
	      <?php foreach ($result as $val): ?>
	      	<div class="col-md-4">
					  <div class="card">
	        		<?php 
					  	if (is_file(IMGPATH.'news'.DS.$val['image'])) $i = URL::site('images/news/big/'.$val['image']);
					  	else $i = URL::site('pic/no-image.png');
					  	?>
						  <img class="card-img-top" src="<?= $i; ?>" alt="">
						  <div class="card-body">
						  	<a href="<?= URL::site('news/'.$val['id']); ?>">
						    	<p class="card-text"><?= $val['name']; ?></p>
						    </a>
						  </div>
						</div>
					</div>
	      <?php endforeach ?>
	    <?php else: ?>
	      <div class="text-center">
		  		<p>Пусто</p>
		  	</div>
	    <?php endif ?>
    </div>
  </div>
  <div class="card-footer">
	  <a href="<?= URL::site(); ?>" class="btn btn-sm btn-secondary">Back</a>
	</div>
</div>