<div class="card mycard">
  <div class="card-header">
	  <?= $result['name']; ?>
	</div>
  <div class="card-body">
    <?= $result['text']; ?>
  </div>
  <div class="card-footer">
	  <a href="<?= URL::site('news'); ?>" class="btn btn-sm btn-secondary">Back</a>
	</div>
</div>