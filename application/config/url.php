<?php

return [

  'trusted_hosts' => [
    'gull.cloudapp.net',
    'localhost:3000',
  ],

];
