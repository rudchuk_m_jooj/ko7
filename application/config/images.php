<?php
// Settings of images on the site
return [
    // Image types
    'types' => [
        'jpg', 'jpeg', 'png'
    ],
    // News images
    'news' => [
        [
            'path' => 'big',
            'width' => 350,
            'height' => 350,
            'resize' => 1,
            'crop' => 1,
        ],
    ],
    // Gallery images
    'gallery' => [
        [
            'path' => 'big',
            'width' => 800,
            'height' => 600,
            'resize' => 1,
            'crop' => 1,
        ],
    ],
];