<?php

class Controller_Control extends Controller_Main {

    public $current;

    public function before()
    {
        parent::before();

        $this->current = Model::factory('Control')->getRowByAlias($this->request->action());

        if (!$this->current) throw new HTTP_Exception_404('Page not found!');

        $this->view->head->set('title', $this->current[0]['name']);
        $this->view->content = View::factory('control/'.$this->request->action())->set('current', $this->current[0]);
    }

    public function action_index()
    {
        $news = View::factory('widgets/news');
        $news->set('result', Model::factory('News')->getRows('DESC', 3));

        $gallery = View::factory('widgets/gallery');
        $gallery->set('result', Model::factory('Gallery')->getRows('ASC', 2));

        $this->view->content->set('news', $news)->set('gallery', $gallery);

        $this->response->body($this->view);
    }

}
