<?php

class Controller_Gallery extends Controller_Main {

  public function action_list()
  {
    $result = Model::factory('Gallery')->getRows('ASC');

    $this->view->head->set('title', 'Gallery');

    $this->view->content = View::factory('gallery/list')->set('result', $result);

    $this->response->body($this->view);
  }

}
