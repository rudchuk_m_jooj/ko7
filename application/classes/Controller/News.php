<?php

class Controller_News extends Controller_Main {

  public function action_list()
  {
    $result = Model::factory('News')->getRows();

    $this->view->head->set('title', 'News');

    $this->view->content = View::factory('news/list')->set('result', $result);

    $this->response->body($this->view);
  }

  public function action_inner()
  {
    $result = Model::factory('News')->getRowByField($this->request->param('id'));

    if (!$result) throw new HTTP_Exception_404('Page not found!');

    $this->view->head->set('title', $result[0]['name']);

    $this->view->content = View::factory('news/inner')->set('result', $result[0]);

    $this->response->body($this->view);
  }

}
