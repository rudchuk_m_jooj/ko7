<?php

class Controller_Main extends Controller {

    public $view;

    public function before()
    {
        parent::before();

        $this->view = View::factory('main');

        $this->view->head = View::factory('widgets/head');
        $this->view->header = View::factory('widgets/header');
        // $this->view->footer = View::factory('widgets/footer');
    }

}
