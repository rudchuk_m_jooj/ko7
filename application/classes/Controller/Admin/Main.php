<?php

class Controller_Admin_Main extends Controller {

    public $view;

    public function before()
    {
        parent::before();

        if(!Auth::instance()->logged_in()) {
            HTTP::redirect('auth');
        }

        $this->view = View::factory('admin/main');

        $this->view->head = View::factory('admin/widgets/head');
        $this->view->header = View::factory('admin/widgets/header');
        $this->view->footer = View::factory('admin/widgets/footer');
    }

}
