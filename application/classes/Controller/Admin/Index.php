<?php

class Controller_Admin_Index extends Controller_Admin_Main {

    public function action_index()
    {
        if ($post = $this->request->post('FORM')) {

            Model::factory('Admin_Control')->updateRow($post, 1);

        }

        $result = Model::factory('Admin_Control')->getRow(1);

        $this->view->head->set('title', 'Edit: '.$result[0]['name']);

        $this->view->content = View::factory('admin/index')->set('result', $result[0]);

        $this->response->body($this->view);
    }

}
