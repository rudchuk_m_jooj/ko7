<?php

class Controller_Admin_Auth extends Controller {

    public function action_login()
    {
        $error = NULL;

        if ($post = $this->request->post()) {

            if(Auth::instance()->login($post['name'], $post['password'])) {

                HTTP::redirect('admin');

            } else $error = 'Login or password incorrect!';

        }

        $this->response->body(View::factory('admin/auth')->set('error', $error));
    }

    public function action_logout()
    {
        Auth::instance()->logout(TRUE);

        HTTP::redirect('auth');
    }

}
