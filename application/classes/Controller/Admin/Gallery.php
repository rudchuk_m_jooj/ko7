<?php

class Controller_Admin_Gallery extends Controller_Admin_Main {

    public function action_index()
    {
        $result = Model::factory('Admin_Gallery')->getRows();

        $this->view->head->set('title', 'List Images');

        $this->view->content = View::factory('admin/gallery/list')->set('result', $result);

        $this->response->body($this->view);
    }

    public function action_edit()
    {
        if ($post = $this->request->post('FORM')) {

            $post['updated_at'] = time();

            if ($filename = File::uploadImg(Model::factory('Admin_Gallery')::$image)) $post['image'] = $filename;

            Model::factory('Admin_Gallery')->updateRow($post, $this->request->param('id'));
        }

        $result = Model::factory('Admin_Gallery')->getRow($this->request->param('id'));

        if (!$result) HTTP::redirect('admin/gallery');

        $this->view->head->set('title', 'Edit Image: '.$result[0]['name']);

        $this->view->content = View::factory('admin/gallery/form')->set('result', $result[0]);

        $this->response->body($this->view);
    }

    public function action_add()
    {
        if ($post = $this->request->post('FORM')) {

            $post['created_at'] = time();

            if ($filename = File::uploadImg(Model::factory('Admin_Gallery')::$image)) $post['image'] = $filename;

            $insert = Model::factory('Admin_Gallery')->insertRow($post);

            HTTP::redirect('admin/gallery/edit/'.$insert[0]);
        }

        $this->view->head->set('title', 'Add Image');

        $this->view->content = View::factory('admin/gallery/form')->set('result', NULL);

        $this->response->body($this->view);
    }

    public function action_delete()
    {
        Model::factory('Admin_Gallery')->deleteRow($this->request->param('id'));

        HTTP::redirect('admin/gallery');
    }

    public function action_deleteImg()
    {
        $result = Model::factory('Admin_Gallery')->getRow($this->request->param('id'));

        if ($result[0] && $result[0]['image']) {
            File::deleteImg(Model::factory('Admin_Gallery')::$image, $result[0]['image']);
        }

        HTTP::redirect('admin/gallery/edit/'.$this->request->param('id'));
    }

}
