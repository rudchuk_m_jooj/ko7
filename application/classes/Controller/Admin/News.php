<?php

class Controller_Admin_News extends Controller_Admin_Main {

    public function action_index()
    {
        $result = Model::factory('Admin_News')->getRows();

        $this->view->head->set('title', 'List News');

        $this->view->content = View::factory('admin/news/list')->set('result', $result);

        $this->response->body($this->view);
    }

    public function action_edit()
    {
        if ($post = $this->request->post('FORM')) {

            $post['date'] = strtotime($post['date']);
            $post['updated_at'] = time();

            if ($filename = File::uploadImg(Model::factory('Admin_News')::$image)) $post['image'] = $filename;

            Model::factory('Admin_News')->updateRow($post, $this->request->param('id'));
        }

        $result = Model::factory('Admin_News')->getRow($this->request->param('id'));

        if (!$result) HTTP::redirect('admin/news');

        $this->view->head->set('title', 'Edit News: '.$result[0]['name']);

        $this->view->content = View::factory('admin/news/form')->set('result', $result[0]);

        $this->response->body($this->view);
    }

    public function action_add()
    {
        if ($post = $this->request->post('FORM')) {

            $post['date'] = strtotime($post['date']);
            $post['created_at'] = time();

            if ($filename = File::uploadImg(Model::factory('Admin_News')::$image)) $post['image'] = $filename;

            $insert = Model::factory('Admin_News')->insertRow($post);

            HTTP::redirect('admin/news/edit/'.$insert[0]);
        }

        $this->view->head->set('title', 'Add News');

        $this->view->content = View::factory('admin/news/form')->set('result', NULL);

        $this->response->body($this->view);
    }

    public function action_delete()
    {
        Model::factory('Admin_News')->deleteRow($this->request->param('id'));

        HTTP::redirect('admin/news');
    }

    public function action_deleteImg()
    {
        $result = Model::factory('Admin_News')->getRow($this->request->param('id'));

        if ($result[0] && $result[0]['image']) {
            File::deleteImg(Model::factory('Admin_News')::$image, $result[0]['image']);
        }

        HTTP::redirect('admin/news/edit/'.$this->request->param('id'));
    }

}
