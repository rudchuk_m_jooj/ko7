<?php 

class Model_Admin_Control extends Model
{
    public static $table = 'controls';

    public function getRow($value = NULL, $field = 'id')
    {
        return DB::select()
            ->from(static::$table)
            ->where(static::$table.'.'.$field, '=', $value)
            ->execute()
            ->as_array();
    }

    public function updateRow($post, $id)
    {
        return DB::update(static::$table)
            ->set($post)
            ->where('id', '=', $id)
            ->execute();
    }

}
