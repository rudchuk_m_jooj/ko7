<?php 

class Model_Admin_Gallery extends Model
{
    public static $table = 'galleries';
    public static $image = 'gallery';

    public function getRows($status = NULL, $sort = 'id', $type = 'DESC', $limit = NULL, $offset = NULL)
    {
        $result = DB::select()->from(static::$table);

        if ($status) {
            $result->where(static::$table.'.status', '=', (int) $status);
        }

        if ($sort) {
            if ($type) $result->order_by(static::$table.'.'.$sort, $type);
            else $result->order_by(static::$table.'.'.$sort);
        }

        if ($limit) {
            $result->limit($limit);
            if ($offset) $result->offset($offset);
        }

        return $result->execute()->as_array();
    }

    public function getRow($value = NULL, $field = 'id')
    {
        return DB::select()
            ->from(static::$table)
            ->where(static::$table.'.'.$field, '=', $value)
            ->execute()
            ->as_array();
    }

    public function updateRow($post, $id)
    {
        return DB::update(static::$table)
            ->set($post)
            ->where('id', '=', $id)
            ->execute();
    }

    public function insertRow($post)
    {
        $keys = $values = [];
        foreach ($post as $key => $value) {
            $keys[] = $key;
            $values[] = $value;
        }
        return DB::insert(static::$table, $keys)->values($values)->execute();
    }

    public function deleteRow($id)
    {
        return DB::delete(static::$table)
            ->where('id', '=', $id)
            ->execute();
    }

}
