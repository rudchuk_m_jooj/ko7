<?php 

class Model_Control extends Model
{
    public static $table = 'controls';

    public function getRowByAlias($alias = NULL)
    {
        return DB::select(
            static::$table.'.name',
            static::$table.'.text'
        )
            ->from(static::$table)
            ->where(static::$table.'.alias', '=', $alias)
            ->execute()
            ->as_array();
    }
}
