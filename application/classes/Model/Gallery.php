<?php 

class Model_Gallery extends Model
{
    public static $table = 'galleries';

    public function getRows($sort_type = 'ASC', $limit = NULL, $offset = NULL)
    {
        $result = DB::select()
            ->from(static::$table)
            ->where(static::$table.'.status', '=', 1)
            ->order_by(static::$table.'.sort', $sort_type);

        if ($limit) {
            $result->limit($limit);
            if ($offset) {
                $result->offset($offset);
            }
        }

        return $result->execute()->as_array();
    }

}
