<?php 

class Model_News extends Model
{
    public static $table = 'news';

    public function getRows($sort_type = 'DESC', $limit = NULL, $offset = NULL)
    {
        $result = DB::select()
            ->from(static::$table)
            ->where(static::$table.'.status', '=', 1)
            ->where(static::$table.'.date', '<=', time())
            ->order_by(static::$table.'.date', $sort_type);

        if ($limit) {
            $result->limit($limit);
            if ($offset) $result->offset($offset);
        }

        return $result->execute()->as_array();
    }

    public function getRowByField($value = NULL, $field = 'id')
    {
        return DB::select()
            ->from(static::$table)
            ->where(static::$table.'.status', '=', 1)
            ->where(static::$table.'.date', '<=', time())
            ->where(static::$table.'.'.$field, '=', $value)
            ->execute()
            ->as_array();
    }

}
