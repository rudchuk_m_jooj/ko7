<?php

class File {

  public static function uploadImg($mainFolder, $name = 'file')
  {
    if(!($_FILES && $_FILES[$name]['name'])) return false;

    $need = Kohana::$config->load('images.'.$mainFolder);
    if(!$need) return false;

    $ext = explode('.', $_FILES[$name]['name'])[1];

    if(!in_array($ext, Kohana::$config->load('images.types'))) return false;

    $filename = md5($_FILES[$name]['name'].'_'.$mainFolder.time()).'.'.$ext;

    $size = getimagesize($_FILES[$name]['tmp_name']);

    foreach($need AS $one) {

      $file = IMGPATH.$mainFolder.DS.$one['path'].DS.$filename;

      $image = Image::factory($_FILES[$name]['tmp_name'], 'GD');

      if($size[0] > $one['width'] || $size[1] > $one['height']) {

        if($one['crop']){

          if( $one['resize'] ){
            $image->resize($one['width'], $one['height'], Image::INVERSE);
          }

          $image->crop($one['width'], $one['height']);

        } else {

          if( $one['resize'] ){
            $image->resize($one['width'], $one['height']/*, Image::INVERSE*/);
          }
  
        }
  
      }
  
      $image->save($file, isset($one['quality'])?$one['quality']:100);

    }

    move_uploaded_file($_FILES[$name]['tmp_name'], IMGPATH.$mainFolder.DS.$filename);

    return $filename;
  }

  public static function deleteImg($mainFolder, $filename)
  {
    if (is_file(IMGPATH.$mainFolder.DS.$filename)) {

      $need = Kohana::$config->load('images.'.$mainFolder);
      if(!$need) return false;

      foreach($need AS $one) {

        unlink(IMGPATH.$mainFolder.DS.$one['path'].DS.$filename);
  
      }

      unlink(IMGPATH.$mainFolder.DS.$filename);

    }

  }

}
